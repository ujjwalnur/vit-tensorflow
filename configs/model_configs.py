import ml_collections


MODEL_CONFIGS = dict()


def _register(get_config):
    config = get_config().lock()
    name = config.get("model_name")
    MODEL_CONFIGS[name] = config
    return get_config


def default_vit_transformer_config():
    config = ml_collections.ConfigDict()
    config.model_name = "ViT-Ti_16"
    config.proj_dim = 192
    config.patch_size = 16
    config.num_layers = 12
    config.mlp_dim = 768
    config.num_heads = 3
    config.dropout_rate = 0.0
    config.attention_dropout_rate = 0.0
    config.classifier_type = "token"
    config.representation_size = None
    return config


def get_ti16_config():
    return default_vit_transformer_config()


def get_s16_config():
    config = default_vit_transformer_config()
    config.model_name = "ViT-S_16"
    config.proj_dim = 384
    config.mlp_dim = 1536
    config.num_heads = 6
    return config


def get_b16_config():
    config = default_vit_transformer_config()
    config.model_name = "ViT-B_16"
    config.proj_dim = 768
    config.mlp_dim = 3072
    config.num_heads = 12
    return config


def get_s16_config():
    config = default_vit_transformer_config()
    config.model_name = "ViT-S_16"
    config.proj_dim = 384
    config.mlp_dim = 1536
    config.num_heads = 6
    return config


def get_l16_config():
    config = default_vit_transformer_config()
    config.model_name = "ViT-L_16"
    config.proj_dim = 1024
    config.mlp_dim = 4096
    config.num_heads = 16
    config.num_layers = 24
    config.attention_dropout_rate = 0.0
    config.dropout_rate = 0.1
    return config
