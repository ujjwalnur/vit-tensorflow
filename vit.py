# TODO: This code currently does not support setting initializers using model configuration. Add support.
from typing import Optional
import tensorflow as tf
from ml_collections import ConfigDict


# image_data_format() must be set by the main function.
CHANNEL_AXIS = 3 if tf.keras.backend.image_data_format() == "channels_last" else 1

BN_AXIS = -1 if tf.keras.backend.image_data_format() == "channels_last" else 1

SPATIAL_AXES = (
    (1, 2) if tf.keras.backend.image_data_format() == "channels_last" else (2, 3)
)


class PatchifyLayer(tf.keras.layers.Layer):
    def __init__(
        self,
        proj_dim: int,
        patch_size: int,
        kernel_init: Optional[tf.keras.initializers.Initializer] = None,
        bias_init: Optional[tf.keras.initializers.Initializer] = None,
        kernel_reg: Optional[tf.keras.regularizers.Regularizer] = None,
        l2_reg: Optional[float] = 0.0,
        name: Optional[str] = None,
    ):
        super(PatchifyLayer, self).__init__(name=name)
        if kernel_init is None:
            kernel_init = tf.keras.initializers.VarianceScaling(
                seed=0
            )  # TODO:Are the default parameters of jax.linen same as those in TF ?

        if bias_init is None:
            bias_init = tf.keras.initializers.Zeros()

        if kernel_reg is not None:
            kernel_reg = tf.keras.regularizers.L2(l2=l2_reg)

        self._embedding = tf.keras.layers.Conv2D(
            filters=proj_dim,
            kernel_size=patch_size,
            strides=patch_size,
            use_bias=True,
            kernel_initializer=kernel_init,
            bias_initializer=bias_init,
            kernel_regularizer=kernel_reg,
            padding="valid",
            name="embedding",
        )

    def call(self, inputs, *args, **kwargs):
        out = self._embedding(inputs)
        out_shape = tf.shape(out)
        out = tf.reshape(
            out,
            shape=(
                out_shape[0],
                out_shape[SPATIAL_AXES[0]] * out_shape[SPATIAL_AXES[1]],
                out_shape[CHANNEL_AXIS],
            ),
        )
        return out


class ClassTokenAdder(tf.keras.layers.Layer):
    def __init__(
        self,
        init: Optional[tf.keras.initializers.Initializer] = None,
        name: Optional[str] = None,
    ):
        super(ClassTokenAdder, self).__init__(name=name)
        self._init = init
        if self._init is None:
            self._init = tf.keras.initializers.Zeros()

    def build(self, input_shape):
        self._init_value = tf.Variable(
            initial_value=tf.zeros(shape=(1, 1, input_shape[-1]))
        )
        self._init_value.assign(self._init(shape=(1, 1, input_shape[-1])))
        return None

    def call(self, inputs, *args, **kwargs):
        inputs_shape = tf.shape(inputs)
        cls_token_init = tf.tile(
            input=self._init_value, multiples=(inputs_shape[0], 1, 1)
        )
        out = tf.concat((cls_token_init, inputs), axis=1)
        return out


class PositionEmbeddingLayer(tf.keras.layers.Layer):
    def __init__(
        self,
        embed_init: Optional[tf.keras.initializers.Initializer] = None,
        name: Optional[str] = None,
    ):
        super(PositionEmbeddingLayer, self).__init__(name=name)
        if embed_init is None:
            embed_init = tf.keras.initializers.RandomNormal(stddev=0.02, seed=0)

        self._embed_init = embed_init

    def build(self, input_shape):
        init_value = self._embed_init(shape=(1, input_shape[1], input_shape[2]))
        self._pe = tf.Variable(
            initial_value=init_value,
            trainable=True,
        )

    def call(self, inputs, *args, **kwargs):
        return self._pe + inputs


class MLPBlock(tf.keras.layers.Layer):
    def __init__(
        self,
        mlp_dim: int,
        out_dim: Optional[int] = None,
        dropout_rate: float = 0.1,
        kernel_init: Optional[tf.keras.initializers.Initializer] = None,
        bias_init: Optional[tf.keras.initializers.Initializer] = None,
        name: Optional[str] = None,
    ):
        super(MLPBlock, self).__init__(name=name)
        self._out_dim = out_dim
        self._kernel_init = kernel_init
        if self._kernel_init is None:
            self._kernel_init = tf.keras.initializers.GlorotUniform(seed=0)
        self._bias_init = bias_init
        if self._bias_init is None:
            self._bias_init = tf.keras.initializers.RandomNormal(stddev=1e-6, seed=0)
        self._hidden = tf.keras.layers.Dense(
            units=mlp_dim,
            kernel_initializer=kernel_init,
            bias_initializer=bias_init,
            activation="gelu",
            name="hidden",
        )
        self._dropout_hidden = tf.keras.layers.Dropout(
            rate=dropout_rate, name="dropout_hidden", seed=0
        )

        self._out_dropout = tf.keras.layers.Dropout(
            rate=dropout_rate, name="dropout_out", seed=0
        )

    def build(self, input_shape):
        self._out_dim = input_shape[-1] if self._out_dim is None else self._out_dim
        self._out = tf.keras.layers.Dense(
            units=self._out_dim,
            kernel_initializer=self._kernel_init,
            bias_initializer=self._bias_init,
            name="mlp_out",
        )

    def call(self, inputs, *args, **kwargs):
        out = self._hidden(inputs)
        out = self._dropout_hidden(out)
        out = self._out(out)
        out = self._out_dropout(out)
        return out


class Encoder1DBlock(tf.keras.layers.Layer):
    def __init__(
        self,
        proj_dim: int,
        mlp_dim: int,
        num_heads: int,
        dropout_rate: Optional[float] = 0.1,
        attention_dropout_rate: Optional[float] = 0.1,
        name: Optional[str] = None,
    ):
        super(Encoder1DBlock, self).__init__(name=name)
        self._num_heads = num_heads
        self._attention_dropout_rate = attention_dropout_rate
        self._layer_norm_pre_mha = tf.keras.layers.LayerNormalization(
            epsilon=1e-6, name="layer_norm_pre_mha"
        )  # epsilon value default is different between jax and TF. we use jax value to be consistent with authors' original code

        self._mha = tf.keras.layers.MultiHeadAttention(
            num_heads=num_heads,
            key_dim=proj_dim // num_heads,
            dropout=attention_dropout_rate,
            name="mha",
        )
        self._dropout = tf.keras.layers.Dropout(
            rate=dropout_rate, seed=0, name="dropout_mha"
        )

        self._layer_norm_post_mha = tf.keras.layers.LayerNormalization(
            name="layer_norm_post_mha"
        )
        self._mlp = MLPBlock(mlp_dim=mlp_dim, dropout_rate=dropout_rate, name="mlp")

    def build(self, input_shape):
        self._mha._build_from_signature(
            query=tf.TensorShape(input_shape), value=tf.TensorShape(input_shape)
        )
        return None

    def call(self, inputs, *args, **kwargs):
        out = self._layer_norm_pre_mha(inputs)
        out = self._mha(out, out)
        out = self._dropout(out)
        out += inputs
        out_mlp = self._layer_norm_post_mha(out)
        out_mlp = self._mlp(out_mlp)
        return out_mlp + out


class ViTEncoder(tf.keras.layers.Layer):
    def __init__(
        self,
        num_layers: int,
        proj_dim: int,
        mlp_dim: int,
        num_heads: int,
        dropout_rate: Optional[float] = 0.1,
        attention_dropout_rate: Optional[float] = 0.1,
        add_position_embeddings: Optional[bool] = True,
        name: Optional[str] = None,
    ):
        super(ViTEncoder, self).__init__(name=name)
        self._modules = tf.keras.Sequential(name="seq")
        if add_position_embeddings:
            self._modules.add(PositionEmbeddingLayer())
            self._modules.add(
                tf.keras.layers.Dropout(rate=dropout_rate, seed=0, name="dropout_pos")
            )

        for layer_num in range(num_layers):
            self._modules.add(
                Encoder1DBlock(
                    mlp_dim=mlp_dim,
                    proj_dim=proj_dim,
                    num_heads=num_heads,
                    dropout_rate=dropout_rate,
                    attention_dropout_rate=attention_dropout_rate,
                    name="encoder_{}".format(layer_num + 1),
                )
            )

        self._modules.add(tf.keras.layers.LayerNormalization(name="encoder_norm"))

    def call(self, inputs, *args, **kwargs):
        out = self._modules(inputs)
        return out


class VisionTransformer(tf.keras.Model):
    def __init__(
        self,
        model_config: ConfigDict,
        num_claases: Optional[int],
        name: Optional[str] = None,
    ):
        super(VisionTransformer, self).__init__(name=name)
        self._num_classes = num_claases
        self._patchify = PatchifyLayer(
            proj_dim=model_config.proj_dim,
            patch_size=model_config.patch_size,
            name="patchify",
        )

        self._encoder = ViTEncoder(
            num_layers=model_config.num_layers,
            proj_dim=model_config.proj_dim,
            mlp_dim=model_config.mlp_dim,
            num_heads=model_config.num_heads,
            dropout_rate=model_config.dropout_rate,
            attention_dropout_rate=model_config.attention_dropout_rate,
            name="vit_encoder",
        )

        self._classifier_type = model_config.classifier_type
        if self._classifier_type in ["token", "token_unpooled"]:
            self._tokenizer = ClassTokenAdder(name="tokenizer")
        else:
            self._tokenizer = tf.keras.layers.Identity(name="tokenizer")

        if model_config.representation_size is not None:
            self._rep_layer = tf.keras.Sequential(
                [
                    tf.keras.layers.Dense(
                        units=model_config.representation_size, name="pre_logits"
                    ),
                    tf.keras.layers.Activation(activation="tanh", name="tanh"),
                ],
                name="representation",
            )
        else:
            self._rep_layer = tf.keras.layers.Identity(name="representation")

        if self._num_classes is not None:
            self._logits = tf.keras.layers.Dense(units=self._num_classes, name="logits")
        else:
            self._logits = tf.keras.layers.Identity()

    def call(self, inputs, training=None, mask=None):
        out = self._patchify(inputs)
        out = self._tokenizer(out)
        out = self._encoder(out)
        if self._classifier_type == "token":
            out = out[:, 0]
        elif self._classifier_type == "gap":
            out = tf.reduce_mean(out, axis=(1,))
        else:
            out = out

        out = self._rep_layer(out)
        out = self._logits(out)
        return out
